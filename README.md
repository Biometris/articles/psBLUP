**psBLUP**
---------------
Proximity-smoothed BLUP, **psBLUP**, is a genomic prediction method that improves the accuracy of the traditional ridge penalty on marker eects in RRBLUP/GBLUP by using additional spatial information on marker locations and forcing marker effects to be more similar when the marker locations are closer. 

## Suitability
The method is suitable for genomic prediction of unphenotyped genotypes in homogeneous plant families (F2, RIL, MAGIC) for phenotypic traits with a low genetic signal to noise ratio in combination with a small training set of genotypes (< 100). 


## Usage
**psBLUP** has been implemented in R. You can use the psBLUP function by using *psBLUP.R* as a source file. For an example of how to use the function, see *Analysis.R*. The method and the analysis are explicated in Bartzis *et al*.


## References

Bartzis, G., Peeters, C.F.W., and van Eeuwijk, F. (under review). psBLUP: Incorporating marker proximity for improving genomic predictin accuracy. 
